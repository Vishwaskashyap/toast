package com.example.dell.toast_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Main_toast extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_toast);
        Button button_1 = (Button) findViewById(R.id.button_1);
        button_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("Button 1 was pressed", "-----View.OnClickListener reached ");
                Toast.makeText(Main_toast.this,"Button 1 was pressed using toast", Toast.LENGTH_LONG).show();
            }

        });

        Button button_2 = (Button) findViewById(R.id.button_2);
        button_2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("Button 2 was pressed", "-----View.OnClickListener reached ");
                Toast.makeText(Main_toast.this, "Button 2 was pressed using toast", Toast.LENGTH_LONG).show();
            }

        });

        Button button_3 = (Button) findViewById(R.id.button_3);
        button_3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("Button 3 was pressed", "-----View.OnClickListener reached ");
                Toast.makeText(Main_toast.this, "Button 3 was pressed using toast", Toast.LENGTH_LONG).show();
            }

        });

    }}
